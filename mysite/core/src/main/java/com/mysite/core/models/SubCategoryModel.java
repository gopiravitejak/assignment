package com.mysite.core.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Session;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SubCategoryModel {
    private final Logger log = LoggerFactory.getLogger(SubCategoryModel.class);

    @Inject
    @Via("resource")
    private String path;

    @Inject
    @Via("resource")
    private String hits;

    @Inject
    @Source("osgi-services")
    QueryBuilder builder;

    @SlingObject
    private ResourceResolver resolver;

    @SlingObject
    private Resource resource;

    @Self
    private SlingHttpServletRequest request;

    private Session session;

    private ArrayList<Demo> result;

    public ArrayList<Demo> getResult() {
        return result;
    }

    public void setResult(ArrayList<Demo> result) {
        this.result = result;
    }

    @PostConstruct
    protected void init() {

        try {
            log.info("----------< Inside Sling Model >----------");

            session = resolver.adaptTo(Session.class);
            Map<String, String> predicate = new HashMap<>();
            predicate.put("path", path); //path from where to read articles
            predicate.put("type", "cq:Page"); // type of page
            predicate.put("orderby", "@jcr:content/cq:lastModified");
            predicate.put("orderby.sort", "desc");
            Query query = builder.createQuery(PredicateGroup.create(predicate), session);
            query.setStart(0);
            query.setHitsPerPage(Integer.valueOf(hits)); // need to pass how many results we want
            SearchResult searchResult = query.getResult();
            log.info("size"+searchResult.getHits().size());
            result=new ArrayList<>();
            for (Hit hit : searchResult.getHits()) {
                String path1 = hit.getPath();
                ValueMap demo = request.getResourceResolver().getResource(path1 + "/jcr:content").adaptTo(ValueMap.class);
               Demo d=new Demo();
                d.setTitle(demo.get("jcr:title").toString());
                d.setTime(demo.get("cq:lastModified",Date.class).toString());
                log.info(d.toString());
                result.add(d);

            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}