package com.mysite.core.models;


import java.util.Date;

public class Demo {

    private String title;

    private String time;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return
                "title='" + title + '\'' +
                ", time='" + time + '\'' ;
    }
}
